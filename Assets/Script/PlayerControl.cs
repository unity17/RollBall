﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

	public float speed;

	private Rigidbody rb;


	// Use this for initialization
	void Start () {

		rb = GetComponent<Rigidbody> ();

	
	}
	
	// Update is called once per frame
	void Update () {

		float moveH = Input.GetAxis ("Horizontal");
		float moveV = Input.GetAxis ("Vertical");

		Vector3 movementInput = new Vector3 (moveH, 0, moveV);


		rb.AddForce (movementInput* speed);
	
	}
}
